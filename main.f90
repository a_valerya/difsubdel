! cd /mnt/c/Users/Rufina/Documents/rufina/VLCMV/DDE/
!

PROGRAM MAIN
  IMPLICIT NONE

  INTEGER N, KPR
  INTEGER NPOINTS, TO_PRINT
  PARAMETER (N = 3, KPR = 1)

  REAL*8 CONSTS(KPR), OBST
  INTEGER KFLAG, I, IEX
  REAL(8) Y(8, N), SOL(N)
  REAL*8 T0

  TO_PRINT = 1
  OBST = 1
  T0 = 0
  IEX = 4
	! Y(1, 1) = 1
  ! Y(1, 2) = 1
  ! Y(1, 3) = 1
	! INIT CONSTS 
  
  CALL SOLVER(T0, OBST, TO_PRINT, KFLAG, IEX)
  ! IF (TO_PRINT > 0) THEN
    ! WRITE(*, '(" T = ", F6.2, 2X, 3D13.6)') OBST, Y(1,1), Y(1,2), Y(1,3)
  ! ENDIF
  ! SOL(1) = Y(1,1);
  ! SOL(2) = Y(1,2);
  ! SOL(3) = Y(1,3);
  IF (KFLAG .LT. 0) THEN
	PRINT *, 'ERROR ', KFLAG
  ENDIF

STOP 
END 
