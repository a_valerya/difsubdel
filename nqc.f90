!******************************************************
! controls the order of method in 'jumps' of max delay
!******************************************************s
SUBROUTINE NQC(UJMP, T, NQ, JSTART, H, HMIN) 
  USE COMMONS
	IMPLICIT NONE
	
	Type(TDISCPT)	:: UJMP
  INTEGER NQ, JSTART
  REAL(8) T, H, HMIN

  INTEGER I

  IF (T .GT. UJMP%TJMAXD(UJMP%MAXD2)) RETURN
  DO I = 1, UJMP%MAXD2
    IF (DABS(T - UJMP%TJMAXD(I)) .LE. (UJMP%U26 * T)) UJMP%INES = I - 1
  END DO
  IF (UJMP%INES .GT. 2) THEN
    NQ = MIN0(NQ, UJMP%INES - 2)
    JSTART = 10
  ELSE
    JSTART = 0
    H = HMIN
  END IF
END
