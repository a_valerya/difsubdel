!******************************************************
! writes some info into extra arrays
!******************************************************
SUBROUTINE DAC(UPRB, UDEL, T, Y, NQ, LDFLFG)
  USE COMMONS
	IMPLICIT NONE

	TYPE(TPROBLEM):: UPRB
	TYPE(TDELAY)	:: UDEL

  INTEGER LDFLFG
  REAL(8) T
  REAL(8) Y(8, UPRB%N)
  INTEGER NQ
  
  INTEGER IC(2), LAST(2), LASTW(2)

  INTEGER I, I4, J, J1, J2, I1, I2, ID, M

  IC(1) = 1
  IC(2) = NQ * UPRB%NYD
  LAST(1) = UDEL%NLAST(UDEL%IDMAX)
  LASTW(1) = UDEL%NLASTW(UDEL%IDMAX)
  I1 = LAST(1)
  I2 = LASTW(1)
  LAST(2) = UDEL%NADEL(I1)
  LASTW(2) = UDEL%NADEL(I2)
  DO I = 1, 2
    IF(LASTW(I) .LT. LAST(I)) THEN
      UDEL%ICAPAC(I) = LASTW(I) - LAST(I) + UDEL%ICAPAC(I) + UDEL%L(I) - 1
    ELSE
      UDEL%ICAPAC(I)=LASTW(I)-LAST(I)+UDEL%ICAPAC(I)
    END IF
    IF (UDEL%ICAPAC(I) .LT. IC(I)) THEN
      WRITE(6, '(1X,"L(",I1,") SIZE TOO SMALL")') I
      LDFLFG = -1
      RETURN
    END IF
  END DO
  I = 4
  I4 = UDEL%ISUCST(1)
  DO
    UDEL%TDEL(I4) = T
    UDEL%NQDEL(I4) = NQ
    UDEL%NADEL(I4) = UDEL%ISUCST(2)
    UDEL%ICAPAC(1) = UDEL%ICAPAC(1) - 1
    UDEL%ISUCST(1) = UDEL%ISUCST(1) + 1
    IF (UDEL%ISUCST(1) .LT. UDEL%L(1)) EXIT
    IF (UDEL%ICAPAC(1) .LT. 2 ) THEN
      WRITE(6, '(1X,"L(",I1,") SIZE TOO SMALL")') I
      LDFLFG = -1
      RETURN
    END IF
    I4 = 1
    UDEL%ISUCST(1) = 1
  END DO
  I = 5
  DO
    J = UDEL%ISUCST(2)
    DO J1 = 1, UPRB%NYD
      ID = UPRB%IYD(J1)
      DO J2 = 1, NQ
        UDEL%YDEL(J) = Y(J2, ID)
        J = J + 1
      END DO
    END DO
    UDEL%ICAPAC(2) = UDEL%ICAPAC(2) - IC(2)
    IF( (UDEL%ISUCST(2) + 16 * UPRB%NYD) .LT. UDEL%L(2) ) EXIT
    IF(  UDEL%ICAPAC(2) .LT. (8 * UPRB%NYD) )  THEN
      WRITE(6, '(1X,"L(",I1,") SIZE TOO SMALL")') I
      LDFLFG = -1
      RETURN
    END IF
    UDEL%ISUCST(2) = 1
    UDEL%NADEL(I4) = 1
  END DO
  UDEL%ISUCST(2) = J
  DO M  =1, UPRB%NDEL
    UDEL%NLAST(M)=UDEL%NLASTW(M)
  END DO
  RETURN
END SUBROUTINE DAC
