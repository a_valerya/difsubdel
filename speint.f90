!******************************************************
! on each iteration calcs new time of exit from interpolator TOUT 
! and new max step size HMAX
!******************************************************
SUBROUTINE SPEINT(UJMP, TPRINT, NPRINT, TOUT, HMAX0, HMAX, TFIN)
  USE COMMONS
	IMPLICIT NONE
	Type(TDISCPT)	:: UJMP
  REAL(8) TPRINT
  INTEGER NPRINT
  REAL(8) TOUT, HMAX0, HMAX, TFIN
  REAL(8) TJ, TOUT0, TOUT1

  HMAX = HMAX0
  TOUT0 = TOUT
  IF (DABS(TOUT - TPRINT * NPRINT) .LE. (TOUT * UJMP%U26)) NPRINT = NPRINT + 1
  TOUT = TPRINT * NPRINT
  TOUT1 = TOUT
  IF (UJMP%INTJ .LT. UJMP%NJUMP - 1) THEN
    IF (TOUT0 .GE. UJMP%TJUMP(UJMP%INTJ + 1)) UJMP%INTJ = UJMP%INTJ + 1
    TJ = UJMP%TJUMP(UJMP%INTJ + 1)
    TOUT = DMIN1(TOUT1, TJ)
    IF (DABS(TJ - TOUT1) .LT. (TJ * UJMP%U26)) THEN
        TOUT = TJ
        NPRINT = NPRINT + 1
    END IF
    HMAX = DMIN1(HMAX, ((UJMP%TJUMP(UJMP%INTJ + 1) - UJMP%TJUMP(UJMP%INTJ)) / 1.1D0))
  END IF
END
