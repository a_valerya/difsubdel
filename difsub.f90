!******************************************************
! single-step integrator 
!******************************************************
! call: diffun, dac, ludec, sol 
SUBROUTINE DIFSUB(UPRB, UDEL, UWRK, UITG, T, Y, HMIN, HMAX, KFLAG, JSTART, LDFLFG)
  USE COMMONS
	IMPLICIT NONE
	
	TYPE(TPROBLEM):: UPRB
	TYPE(TWORK)		:: UWRK
	TYPE(TDELAY)	:: UDEL
	TYPE(TINTEG) 	:: UITG
	
  REAL(8) T, Y(8, UPRB%N), HMIN, HMAX
  INTEGER KFLAG, JSTART, LDFLFG

  REAL(8) PERTST(7,2,3)
  DATA PERTST / 2.0D0,  4.5D0, 7.333D0,  10.42D0,    13.7D0,    17.15D0,    1.0D0,&
                2.0D0, 12.0D0,  24.0D0,  37.89D0,   53.33D0,    70.08D0,  87.97D0,&
                3.0D0,  6.0D0, 9.167D0,   12.5D0,   15.98D0,      1.0D0,    1.0D0,&
               12.0D0, 24.0D0, 37.89D0,  53.33D0,   70.08D0,    87.97D0,    1.0D0,&
                1.0D0,  1.0D0,   0.5D0, 0.1667D0, 0.04133D0, 0.008267D0,    1.0D0,&
                1.0D0,  1.0D0,   2.0D0,    1.0D0,  0.3157D0,  0.07407D0, 0.0139D0/

  REAL(8) RACUM, D, HFCN, PEPSH, PR1, PR2, PR3, R, R1
  INTEGER IRET, I, IER, IRET1, J, J1, J2, L1, N11, N12, NT
  LOGICAL INIT, NEWITER, TERMINATE, EXTERMINATE
  RACUM = 1.D0
  IRET = 1
  KFLAG = 1
  UWRK%INDJ = 0
  IF (JSTART .NE. -1) THEN
    IF (JSTART .LE. 0) THEN
      UWRK%NQ = 1
      UWRK%N3 = UPRB%N
      UWRK%N1 = UPRB%N * 10
      UWRK%N2 = UWRK%N1 + 1
      UWRK%N4 = UPRB%N ** 2
      UWRK%N5 = UWRK%N1 + UPRB%N
      UWRK%N6 = UWRK%N5 + 1
      UWRK%INDJ = 1
      UWRK%INTCON = 1
      HFCN = 0.D0
      CALL DIFFUN(UPRB, UDEL, UWRK, T, Y, UITG%YP1, LDFLFG)
      IF (LDFLFG .NE. 0) RETURN
      UWRK%INDJ = 0
      IF (T .EQ. UWRK%T0) CALL DAC(UPRB, UDEL, T, Y, UWRK%NQ + 1, LDFLFG)
      IF (LDFLFG .NE. 0) RETURN
      DO I = 1, UPRB%N
      Y(2,I) = UITG%YP1(I) * UWRK%H_W
      END DO
      UWRK%HNEW = UWRK%H_W
      UWRK%K_W = 2
    END IF
    DO I = 1, UPRB%N
      DO J = 1, UWRK%K_W
        UITG%SAVER(J, I) = Y(J, I)
      END DO
    END DO
    UWRK%HOLD = UWRK%HNEW
    IF (UWRK%H_W .NE. UWRK%HOLD) THEN
      UWRK%IWEVAL = UITG%MF
      RACUM = UWRK%H_W / UWRK%HOLD
      RACUM = DMAX1(DABS(HMIN / UWRK%HOLD), RACUM)
      RACUM = DMIN1(RACUM, DABS(HMAX / UWRK%HOLD))
      R1 = 1.D0
      DO J = 2, UWRK%K_W
        R1 = R1 * RACUM
        DO I = 1, UPRB%N
          Y(J, I) = UITG%SAVER(J, I) * R1
        END DO
      END DO
      UWRK%H_W = UWRK%HOLD * RACUM
      DO I = 1, UPRB%N
        Y(1, I) = UITG%SAVER(1, I)
      END DO
      UWRK%IDOUB = UWRK%K_W
    END IF
  ELSE
    IF (UWRK%NQ .EQ. UWRK%NQOLD) JSTART = 1
    T = UWRK%TOLD
    UWRK%NQ = UWRK%NQOLD
    UWRK%K_W = UWRK%NQ + 1
    RACUM = UWRK%H_W / UWRK%HOLD
    RACUM = DMAX1(DABS(HMIN / UWRK%HOLD), RACUM)
    RACUM = DMIN1(RACUM, DABS(HMAX / UWRK%HOLD))
    R1 = 1.D0
    DO J = 2, UWRK%K_W
      R1 = R1 * RACUM
      DO I = 1, UPRB%N
        Y(J, I) = UITG%SAVER(J, I) * R1
      END DO
    END DO
    UWRK%H_W = UWRK%HOLD * RACUM
    DO I = 1, UPRB%N
      Y(1, I) = UITG%SAVER(1, I)
    END DO
    UWRK%IDOUB = UWRK%K_W
  END IF
  UWRK%NQOLD = UWRK%NQ
  UWRK%TOLD = T
  INIT = .TRUE.
  EXTERMINATE = .FALSE.
  IF ((JSTART .GT. 0) .AND. (JSTART .NE. 10)) INIT = .FALSE.
  IF(JSTART.EQ.10) JSTART = UWRK%NQ
  DO
    IF (INIT) THEN
      IF (UITG%MF .NE. 0) THEN
        IF ((UWRK%NQ .GT. 6) .OR. (UITG%MAXDER .GT. 6)) THEN
          KFLAG = -2
          RETURN
        END IF
        SELECT CASE(UWRK%NQ)
        CASE(1)
          UITG%A(1) = -1.0D0
        CASE(2)
          UITG%A(1) = -0.6666666666666667D0
          UITG%A(3) = -0.3333333333333333D0
        CASE(3)
          UITG%A(1) = -0.54545454545454545D0
          UITG%A(3) = UITG%A(1)
          UITG%A(4) = -0.09090909090909091D0
        CASE(4)
          UITG%A(1) = -0.48D0
          UITG%A(3) = -0.70D0
          UITG%A(4) = -0.20D0
          UITG%A(5) = -0.02D0
        CASE(5)
          UITG%A(1) = -0.437956204379562D0
          UITG%A(3) = -0.8211678832116788D0
          UITG%A(4) = -0.3102189781021898D0
          UITG%A(5) = -0.5474452554744526D-1
          UITG%A(6) = -0.36496350364963504D-2
        CASE(6)
          UITG%A(1) = -0.4081632653061225D0
          UITG%A(3) = -0.9206349206349206D0
          UITG%A(4) = -0.4166666666666667D0
          UITG%A(5) = -0.0992063492063492D0
          UITG%A(6) = -0.0119047619047619D0
          UITG%A(7) = -0.566893424036282D-3
        END SELECT
      ELSE
        IF ((UWRK%NQ .GT. 7) .OR. (UITG%MAXDER .GT. 7)) THEN
          KFLAG = -2
          RETURN
        END IF
        SELECT CASE(UWRK%NQ)
        CASE(1)
          UITG%A(1) = -1.D0
        CASE(2)
          UITG%A(1) = -0.5D0
          UITG%A(3) = -0.5D0
        CASE(3)
          UITG%A(1) = -0.4166666666666667D0
          UITG%A(3) = -0.75D0
          UITG%A(4) = -0.1666666666666667D0
        CASE(4)
          UITG%A(1) = -0.375D0
          UITG%A(3) = -0.9166666666666667D0
          UITG%A(4) = -0.3333333333333333D0
          UITG%A(5) = -0.4166666666666667D-1
        CASE(5)
          UITG%A(1) = -0.3486111111111111D0
          UITG%A(3) = -1.0416666666666667D0
          UITG%A(4) = -0.4861111111111111D0
          UITG%A(5) = -0.1041666666666667D0
          UITG%A(6) = -0.8333333333333333D-2
        CASE(6)
          UITG%A(1) = -0.3298611111111111D0
          UITG%A(3) = -1.1416666666666667D0
          UITG%A(4) = -0.625D0
          UITG%A(5) = -0.1770833333333333D0
          UITG%A(6) = -0.025D0
          UITG%A(7) = -0.1388888888888889D-2
        CASE(7)
          UITG%A(1) = -0.3155919312169312D0
          UITG%A(3) = -1.225D0
          UITG%A(4) = -0.7518518518518519D0
          UITG%A(5) = -0.2552083333333333D0
          UITG%A(6) = -0.4861111111111111D-1
          UITG%A(7) = -0.4861111111111111D-2
          UITG%A(8) = -0.1984126984126984D-3
        END SELECT
      END IF
      UWRK%K_W = UWRK%NQ + 1
      UWRK%IDOUB = UWRK%K_W
      UITG%A(2) = -1.D0
      UWRK%MTYP = (4 - UITG%MF) / 2
      UWRK%ENQ2 = 0.5D0 / DBLE(UWRK%NQ + 1)
      UWRK%ENQ3 = 0.5D0 / DBLE(UWRK%NQ + 2)
      UWRK%ENQ1 = 0.5D0 / DBLE(UWRK%NQ)
      PEPSH = UITG%EPS
      UWRK%EUP = (PERTST(UWRK%NQ, UWRK%MTYP, 2) * PEPSH) ** 2
      UWRK%E = (PERTST(UWRK%NQ, UWRK%MTYP, 1) * PEPSH) ** 2
      UWRK%EDWN = (PERTST(UWRK%NQ, UWRK%MTYP, 3) * PEPSH) ** 2
      IF (UWRK%EDWN .EQ. 0.0D0) THEN
        KFLAG = -4
        DO I = 1, UPRB%N
          DO J = 1, UWRK%K_W
            Y(J, I) = UITG%SAVER(J, I)
          END DO
        END DO
        UWRK%H_W = UWRK%HOLD
        UWRK%NQ = UWRK%NQOLD
        JSTART = UWRK%NQ
        RETURN
      END IF
      UWRK%BND = UITG%EPS * UWRK%ENQ3 / DBLE(UPRB%N)
      UWRK%IWEVAL = UITG%MF
      IF (IRET .EQ. 2) EXIT
    END IF
    T = T + UWRK%H_W
    DO J = 2, UWRK%K_W
      DO J1 = J, UWRK%K_W
        J2 = UWRK%K_W - J1 + J - 1
        DO I = 1, UPRB%N
          Y(J2,I)=Y(J2,I)+Y(J2+1,I)
        END DO
      END DO
    END DO
    DO I = 1, UPRB%N
      UITG%ERROR(I) = 0.0
    END DO
    UWRK%INTCON = 1
    NEWITER = .FALSE.
    TERMINATE = .FALSE.
    EXTERMINATE = .FALSE.
    DO L1 = 1, 3
      CALL DIFFUN(UPRB, UDEL, UWRK, T, Y, UITG%YP1, LDFLFG)
      IF (LDFLFG .NE. 0) RETURN
      IF (UWRK%IWEVAL .GE. 1) THEN
        IF (UITG%MF .EQ. 2) THEN
          DO I = 1, UPRB%N
            UITG%SAVER(9, I) = Y(1, I)
          END DO
          DO J = 1,UPRB%N
            R = UITG%EPS * DMAX1(UITG%EPS, DABS(UITG%SAVER(9, J)))
            Y(1, J) = Y(1, J) + R
            D = UITG%A(1) * UWRK%H_W / R
            CALL DIFFUN(UPRB, UDEL, UWRK, T, Y, UITG%YP2, LDFLFG)
            IF (LDFLFG .NE. 0) RETURN
            DO I = 1, UPRB%N
              N11 = I + (J - 1) * UWRK%N3
              UITG%PW(N11) = (UITG%YP2(I) - UITG%YP1(I)) * D
            END DO
            Y(1, J) = UITG%SAVER(9, J)
          END DO
        ELSE
          CALL PEDERV(UPRB%N, T, Y, UITG%PW, UWRK%N3)
          R = UITG%A(1) * UWRK%H_W
          DO J = 1, UWRK%N4
            UITG%PW(I) = UITG%PW(I) * R
          END DO
        END IF
        N11 = UWRK%N3 + 1
        N12 = UPRB%N * N11 - UWRK%N3
        DO I = 1, N12, N11
          UITG%PW(I) = 1.0D0 + UITG%PW(I)
        END DO
        UWRK%IWEVAL = -1
        CALL LUDEC(UITG, UPRB%N, IER)
        IF (IER .NE. 0) EXIT
      END IF
      IF (UITG%MF .EQ. 0) THEN
        DO I = 1, UPRB%N
        UITG%SAVER(9, I) = Y(2, I) - UITG%YP1(I) * UWRK%H_W
        END DO
      ELSE
        DO I = 1, UPRB%N
          UITG%YP2(I) = Y(2, I) - UITG%YP1(I) * UWRK%H_W
        END DO
        CALL SOL(UITG, UPRB%N, UITG%YP2)
        DO I = 1, UPRB%N
          UITG%SAVER(9, I) = UITG%YP2(I)
        END DO
      END IF
      NT = UPRB%N
      DO I = 1, UPRB%N
         Y(1, I) = Y(1,I) + UITG%A(1) * UITG%SAVER(9, I)
         Y(2, I) = Y(2, I) - UITG%SAVER(9, I)
         UITG%ERROR(I) = UITG%ERROR(I) + UITG%SAVER(9, I)
         IF (DABS(UITG%SAVER(9, I)) .LE. (UWRK%BND * UITG%YMAX(I))) NT = NT - 1
      END DO
      IF (NT .LE. 0) THEN
        D = 0.D0
        DO I = 1, UPRB%N
          D = D + (UITG%ERROR(I) / UITG%YMAX(I)) ** 2
        END DO
        UWRK%IWEVAL = 0
        IF (D .GT. UWRK%E) THEN
          KFLAG = KFLAG - 2
          IF (UWRK%H_W .LE. (HMIN * 1.00001D0)) THEN
            KFLAG = -1
            UWRK%HNEW = UWRK%H_W
            JSTART = UWRK%NQ
            RETURN
          END IF
          T = UWRK%TOLD
          IF (KFLAG .LE. -5) THEN
            IF ((UWRK%NQ .EQ. 1) .AND. (KFLAG .LT. (1 - UITG%NCUT * 2))) THEN
              KFLAG = -4
              DO I = 1, UPRB%N
              DO J = 1, UWRK%K_W
              Y(J, I) = UITG%SAVER(J, I)
              END DO
              END DO
              UWRK%H_W = UWRK%HOLD
              UWRK%NQ = UWRK%NQOLD
              JSTART = UWRK%NQ
              RETURN
            END IF
            CALL DIFFUN(UPRB, UDEL, UWRK, T, Y, UITG%YP1, LDFLFG)
            IF (LDFLFG .NE. 0) RETURN
            R = 0.1D0
            R = DMAX1(HMIN / DABS(UWRK%H_W), R)
            UWRK%H_W = UWRK%H_W * R
            UWRK%IWEVAL = UITG%MF
            DO I = 1, UPRB%N
            Y(1, I) = UITG%SAVER(1, I)
            UITG%SAVER(2, I) = UWRK%H_W * UITG%YP1(I)
            Y(2, I) = UWRK%H_W * UITG%YP1(I)
            END DO
            UWRK%HOLD = UWRK%H_W
            RACUM = 1.D0
            NEWITER = .TRUE.
            IF (UWRK%NQ .EQ. 1) THEN
              INIT = .FALSE.
            ELSE
              UWRK%NQ = 1
              IRET = 1
              INIT = .TRUE.
            END IF
            EXIT
          END IF
        ELSE
          CALL DAC(UPRB, UDEL, T, Y, UWRK%K_W, LDFLFG)
          IF (LDFLFG .NE. 0) RETURN
          IF (UWRK%K_W .GE. 3) THEN
            DO J = 3, UWRK%K_W
              DO I = 1, UPRB%N
                Y(J, I) = Y(J, I) + UITG%A(J) * UITG%ERROR(I)
              END DO
            END DO
          END IF
          KFLAG = +1
          UWRK%HNEW = UWRK%H_W
          IF (UWRK%IDOUB .GT. 1) THEN
            UWRK%IDOUB = UWRK%IDOUB - 1
            IF (UWRK%IDOUB .LE. 1) THEN
              DO I = 1, UPRB%N
                UITG%SAVER(10,I)=UITG%ERROR(I)
              END DO
            END IF
            EXTERMINATE = .TRUE.
            EXIT
          END IF
        END IF
        PR2 = (D / UWRK%E) ** UWRK%ENQ2 * 1.2D0
        PR3 = 1.D+20
        IF ((UWRK%NQ .LT. UITG%MAXDER) .AND. (KFLAG .GT. -1)) THEN
          D = 0.D0
          DO I = 1, UPRB%N
            D = D + ((UITG%ERROR(I) - UITG%SAVER(10, I)) / UITG%YMAX(I)) ** 2
          END DO
          PR3 = (D / UWRK%EUP) ** UWRK%ENQ3 * 1.4D0
        END IF
        PR1 = 1.D+20
        IF (UWRK%NQ .GT. 1) THEN
          D = 0.D0
          DO I = 1, UPRB%N
            D = D + (Y(UWRK%K_W, I) / UITG%YMAX(I)) ** 2
          END DO
          PR1 = (D / UWRK%EDWN) ** UWRK%ENQ1 * 1.3D0
        END IF
        IF (PR2 .LE. PR3) THEN
          IF (PR2 .GT. PR1) THEN
            R = 1.D0 / DMAX1(PR1, 1.D-4)
            UWRK%NEWQ = UWRK%NQ - 1
          ELSE
            UWRK%NEWQ = UWRK%NQ
            R = 1.D0 / DMAX1(PR2, 1.D-4)
          END IF
        ELSE IF (PR3 .LT. PR1) THEN
          R = 1.D0 / DMAX1(PR3, 1.D-4)
          UWRK%NEWQ = UWRK%NQ + 1
        END IF
        UWRK%IDOUB = 10
        IF ((KFLAG .EQ. 1) .AND. (R .LT. 1.1D0)) THEN
          EXTERMINATE = .TRUE.
          EXIT
        END IF
        IF (UWRK%NEWQ .GT. UWRK%NQ) THEN
          DO I = 1, UPRB%N
            Y(UWRK%NEWQ + 1, I) = UITG%ERROR(I) * UITG%A(UWRK%K_W) / DBLE(UWRK%K_W)
          END DO
        END IF
        UWRK%K_W = UWRK%NEWQ + 1
        IF (KFLAG .NE. 1) THEN
          RACUM = RACUM * R
          RACUM = DMAX1(DABS(HMIN / UWRK%HOLD), RACUM)
          RACUM = DMIN1(RACUM, DABS(HMAX / UWRK%HOLD))
          R1 = 1.D0
          DO J = 2, UWRK%K_W
            R1 = R1 * RACUM
            DO I = 1, UPRB%N
              Y(J, I) = UITG%SAVER(J, I) * R1
            END DO
          END DO
          UWRK%H_W = UWRK%HOLD * RACUM
          DO I = 1, UPRB%N
            Y(1, I) = UITG%SAVER(1, I)
          END DO
          UWRK%IDOUB = UWRK%K_W
          NEWITER = .TRUE.
          IF (UWRK%NEWQ .EQ. UWRK%NQ) THEN
            INIT = .FALSE.
          ELSE
            UWRK%NQ = UWRK%NEWQ
            INIT = .TRUE.
          END IF
          EXIT
        END IF
        IRET = 2
        R = DMIN1(R, HMAX / DABS(UWRK%H_W))
        UWRK%H_W = UWRK%H_W * R
        UWRK%HNEW = UWRK%H_W
        IF (UWRK%NQ .EQ. UWRK%NEWQ) THEN
          TERMINATE = .TRUE.
        ELSE
          UWRK%NQ = UWRK%NEWQ
          NEWITER = .TRUE.
          INIT = .TRUE.
        END IF
        EXIT
      END IF
    END DO
    IF (NEWITER) CYCLE
    IF (TERMINATE .OR. EXTERMINATE) EXIT
    T = T - UWRK%H_W
    IF ((UWRK%H_W .GT. (HMIN * 1.00001D0)) .OR. ((UWRK%IWEVAL - UWRK%MTYP) .GE. -1)) THEN
      IF ((UITG%MF .EQ. 0) .OR. (UWRK%IWEVAL .NE. 0)) RACUM = RACUM * 0.25D0
      UWRK%IWEVAL = UITG%MF
      RACUM = DMAX1(DABS(HMIN / UWRK%HOLD), RACUM)
      RACUM = DMIN1(RACUM, DABS(HMAX / UWRK%HOLD))
      R1 = 1.D0
      DO J = 2, UWRK%K_W
        R1 = R1 * RACUM
        DO I = 1, UPRB%N
          Y(J, I) = UITG%SAVER(J, I) * R1
        END DO
      END DO
      UWRK%H_W = UWRK%HOLD * RACUM
      DO I = 1, UPRB%N
      Y(1, I) = UITG%SAVER(1, I)
      END DO
      UWRK%IDOUB = UWRK%K_W
      INIT = .FALSE.
      CYCLE
    END IF
    KFLAG=-3
    DO I = 1, UPRB%N
      DO J = 1, UWRK%K_W
        Y(J, I) = UITG%SAVER(J, I)
      END DO
    END DO
    UWRK%H_W = UWRK%HOLD
    UWRK%NQ = UWRK%NQOLD
    JSTART = UWRK%NQ
    RETURN
  END DO
  IF (.NOT. EXTERMINATE) THEN
    R1 = 1.D0
    DO J = 2, UWRK%K_W
      R1 = R1 * R
      DO I = 1, UPRB%N
        Y(J, I) = Y(J, I) * R1
      END DO
    END DO
    UWRK%IDOUB = UWRK%K_W
  END IF
  DO I = 1, UPRB%N
    UITG%YMAX(I) = DMAX1(UITG%GROUND(I), DABS(Y(1, I)))
  END DO
  JSTART = UWRK%NQ
  RETURN
END
