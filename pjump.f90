!******************************************************
! works before integration starts,  
! searches for possible derivatives discontinuity points ('jumps')
! sorts the points from min to max 
! UJMP%NJUMP on enter -- possible num of jumps, on return -- actual num of different jumps
!******************************************************
SUBROUTINE PJUMP(UJMP, UWRK, NDEL, DELAY, IDMAX)
  USE COMMONS
	IMPLICIT NONE

	Type(TDISCPT)	:: UJMP
	TYPE(TWORK)		:: UWRK
	
  INTEGER NDEL, IDMAX
  REAL(8) DELAY(NDEL), TJWORK(200)

  INTEGER I, J, K, IDEX, NJUMP1
  REAL(8) EPS, EPSP1, VALVE, XJ

  IDMAX = 1
  UJMP%IDMIN = 1

  IF (NDEL .NE. 1) THEN
    DO I = 1, NDEL - 1
      IF (DELAY(I + 1) .GT. DELAY(IDMAX)) IDMAX = I + 1
      IF (DELAY(I + 1) .LT. DELAY(UJMP%IDMIN)) UJMP%IDMIN = I + 1
    END DO
  END IF

  DO I = 1, UJMP%MAXD2 + 1
    UJMP%TJMAXD(I) = DELAY(IDMAX) * (I - 1) + UWRK%T0
  END DO

  NJUMP1 = UJMP%NJUMP
  UJMP%TJUMP(1) = UWRK%T0
  K = 1
  DO I = 1, UJMP%NJUMP
    TJWORK(I) = UWRK%T0
  END DO

  DO I = 1, NDEL
    DO J = 1, UJMP%MAXD2
      K = K + 1
      XJ = DBLE(J)
      UJMP%TJUMP(K) = DELAY(I) * XJ + UWRK%T0
    END DO
  END DO

  EPS = 1.D0
  EPSP1 = 2.D0
  DO WHILE (EPSP1 .GT. 1D0)
    EPS = EPS / 2.D0
    EPSP1 = EPS + 1.D0
  END DO
  UJMP%U26 = 26.D0 * EPS
  IDEX = 0
  TJWORK(1) = UJMP%TJUMP(1)
  K = 1
  DO I = 2, UJMP%NJUMP
    DO J = 1, I
      IF (DABS(UJMP%TJUMP(I) - TJWORK(J)) .LT. UJMP%TJUMP(I) * UJMP%U26) IDEX = 1
    END DO
    IF (IDEX .EQ. 0) THEN
      K = K + 1
      TJWORK(K) = UJMP%TJUMP(I)
    END IF
    IDEX = 0
  END DO
  UJMP%NJUMP = K
  IDEX = 1
  DO WHILE (IDEX .NE. 0)
    IDEX = 0
    DO I = 1, K - 1
      IF (TJWORK(I) .GT. TJWORK(I + 1)) THEN
        VALVE = TJWORK(I)
        TJWORK(I) = TJWORK(I + 1)
        TJWORK(I + 1) = VALVE
        IDEX = 1
      END IF
    END DO
  END DO
  DO I = 1, K
    UJMP%TJUMP(I) = TJWORK(I)
  END DO
  UJMP%NJUMP = K
  RETURN
END
