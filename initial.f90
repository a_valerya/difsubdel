!******************************************************
! sets initial values for delayed vars
! T - time, passed only T < T0
! IY - index of delayed var 1 <= IY <= N 
! result Y(IY) at time  T < T0
! sub was changed and now is independent of delay index 
!******************************************************
SUBROUTINE INITIAL(T, IY, YD1)
  IMPLICIT NONE
  REAL(8) T, YD1
  INTEGER IY
	
!*==========================================================================
!********************* INITIAL FUNCTIONS SET BY USER ***********************
	SELECT CASE(IY)
    CASE(1)
			YD1 = 1.0
		CASE(2)
			YD1 = 0.0
		CASE(3)
			YD1 = 0.0
!		CASE DEFAULT 
!			YD1 = 0.0
    END SELECT
!***************************************************************************	
!*==========================================================================

END SUBROUTINE INITIAL
