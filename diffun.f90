!******************************************************
! DDE system: T - time, Y(1,N) - current Y value (input)
! YP(N) - derivative value (output) 
! YD, VALUE_1 - delayed vars 
!******************************************************
! call: deint
SUBROUTINE DIFFUN(UPRB, UDEL, UWRK, T,Y,YP, LDFLFG)
  USE COMMONS
	IMPLICIT NONE
	
	TYPE(TPROBLEM):: UPRB
	TYPE(TWORK)		:: UWRK
	TYPE(TDELAY)	:: UDEL

  REAL(8) T, Y(8, UPRB%N), YP(UPRB%N), YD(UPRB%NYD)
  INTEGER LDFLFG
  INTEGER I, II, III

  IF ((UWRK%INTCON .NE. 0) .OR. (UWRK%H_W .GT. UWRK%DELMIN)) THEN
    DO I = 1, UPRB%NDEL
      IF((UWRK%INTCON .NE. 0) .OR. (UWRK%H_W .GT. UPRB%DELAY(I))) THEN
        CALL DEINT(UPRB, UDEL, UWRK, T, I, Y, YD, LDFLFG)
        DO II = 1, UPRB%ILENG(I)
          III = UPRB%IYD(UPRB%IVDS(II,I))
          UDEL%VALUE_1( III, I ) = YD(II)
        END DO
      END IF
    END DO
  END IF
	UWRK%INTCON = 0

!*==========================================================================
!************************ DDE SYSTEM SET BY USER ***************************
  YP(1) = UDEL%VALUE_1(1,1) + UDEL%VALUE_1(3,1) + UDEL%VALUE_1(3,2)
  YP(2) = UDEL%VALUE_1(1,3) + UDEL%VALUE_1(3,4) * Y(1,2)
  YP(3) = Y(1,3)
!***************************************************************************	
!*==========================================================================

END SUBROUTINE DIFFUN
