MODULE COMMONS
	IMPLICIT NONE
	! PROBLEM AND DDE SYSTEM PARAMETERS
	TYPE TPROBLEM
		INTEGER                       :: N 			! num of equations y_i
		INTEGER                       :: NDEL 	! num of unique delays t_i
		INTEGER                       :: NYD  	! num of y-vars which are delayed 
		INTEGER                       :: KPR		! max num of equation parameters 
		REAL(8),POINTER,DIMENSION(:)	:: RPAR		! (KPR) array of equation parameters 
		REAL(8),POINTER,DIMENSION(:)	:: DELAY	! (NDEL) array of delays 
		INTEGER,POINTER,DIMENSION(:)	:: ILENG	! (NDEL) array with num of delayed y-vars with this delay  
		INTEGER,POINTER,DIMENSION(:)	:: IYD		! (NYD) index of y-var related to delayed vars
		INTEGER,POINTER,DIMENSION(:,:):: IVDS		! (NYD, NDEL) array of delayed vars for each delay 
		INTEGER,POINTER,DIMENSION(:,:):: MD			! (N, NDEL) array of flags if y_i(t - tau_j) in system 
	END TYPE TPROBLEM 
   
    ! DELAY DATA, EXTRA ARRAYS TO CALCULATE DELAYS
  TYPE TDELAY 
		! COMMON/DEL1/ !
		INTEGER,POINTER,DIMENSION(:)	:: NLAST	! (NDEL) extra delay info  
		INTEGER,POINTER,DIMENSION(:)	:: NLASTW	! (NDEL) extra delay info 
		INTEGER                       :: IDMAX	! index of max delay 
		INTEGER                       :: ICAPAC(2)! extra delay info 
		INTEGER                       :: ISUCST(2)! extra delay info 
		! COMMON/DEL2/ !
		REAL(8),POINTER,DIMENSION(:)	:: TDEL		! (MXTDEL) array of delays on mesh
		REAL(8),POINTER,DIMENSION(:)	:: YDEL		! (MXYDEL) array of y-vars, and derivatives on mesh
		INTEGER,POINTER,DIMENSION(:)	:: NQDEL	! (MXTDEL) array of NQ values on mesh  
		INTEGER,POINTER,DIMENSION(:)	:: NADEL	! (MXTDEL) array of pointers to YDEL 
		INTEGER												:: L(2)		! = MXTDEL, MXYDEL
		! COMMON/DEL3/ !
		REAL(8),POINTER,DIMENSION(:,:)	:: VALUE_1	! (N, NDEL) values of delayed vars 		
	END TYPE TDELAY
	 
	! INTEGRATOR WORKING VARIABLES 
	TYPE TWORK
		! COMMON/WORK/ !
		REAL(8)	:: HOLD, HNEW, E, BND, EUP, EDWN, TOLD, ENQ1, ENQ2, ENQ3
		INTEGER :: IDOUB, IWEVAL, MTYP, N1, N2, N3, N4, N5, N6, NQ, NEWQ, NQOLD
		! COMMON/WORK1/ !
		REAL(8) :: H_W			! H - integration step 
		REAL(8) :: DELMIN	! 
		INTEGER :: K_W			!  
		INTEGER :: INTCON	! UWRK%INTCON = 1 - first step, 0 - next step  
		! COMMON/WORK2/ !
		REAL(8) :: T0
		INTEGER :: INDJ
	END TYPE TWORK
	
	! JUMPS = derivatives discontinuity points
	TYPE TDISCPT 
		INTEGER												:: MXJMP	! original size of arrays for preallocation,  
		INTEGER												:: NJUMP 	! num of possible jumps, actual size or UJMP%TJUMP 
		INTEGER												:: MAXD2 	! actual size of UJMP%TJMAXD
		REAL(8),POINTER,DIMENSION(:)	:: TJUMP 	! (MXJMP) array of all unique jumps
		REAL(8),POINTER,DIMENSION(:)	:: TJMAXD	! (MXJMP) array of jumps corresponding max delay 
		REAL(8)												:: U26		! =26*eps, relative error norm 
		INTEGER												:: IDMIN,INTJ,INES	! extra vars 
	END TYPE TDISCPT
	
	! integrator work arrays, saving arrays and parameters
	TYPE TINTEG
		REAL(8),POINTER,DIMENSION(:,:)	:: SAVER	! (10,N) saves Y values 
		REAL(8),POINTER,DIMENSION(:,:)	:: ALU		! (N, N) matrix for LUDEC and SOL 
		REAL(8),POINTER,DIMENSION(:)		:: YMAX		! (N)
		REAL(8),POINTER,DIMENSION(:)		:: ERROR	! (N) 
		REAL(8),POINTER,DIMENSION(:)		:: GROUND	! (N) zeros 
		REAL(8),POINTER,DIMENSION(:)		:: PW			! (N*N) matrix ALU to vector 
		REAL(8),POINTER,DIMENSION(:)		:: YP1		! (N) stores Y derivatives 
		REAL(8),POINTER,DIMENSION(:)		:: YP2		! (N) stores Y derivatives 
		INTEGER,POINTER,DIMENSION(:)		:: IP			! (N) permutaion vector for LUDEC and SOL 
		REAL(8)													:: A(8)		! numeric scheme coefs
		REAL(8)													:: EPS		! machine error 
		INTEGER													:: MF			! integration method: 0 - ABM, 
																							! 1 - BDF (with analytical Jacobian), 2 - BDF (with numeric Jacobian)
		INTEGER													:: MAXDER	! 6 or 7, max order of method 
		INTEGER													:: NCUT		! max num of step size H gradual reduction 
	END TYPE TINTEG
END MODULE COMMONS		


  ! переменная NPW = N*N  удалена
	! переменные оставшиеся в solver
  ! Y(8,N) - вектор переменных и их производных 
	! T - время в данной расчетной точке 
	! T0S, TFIN - входные параметры, время начала и конца расчета 
	! TOUT - точка выхода DIFSUB
	! NPRINT - счетчик для печати результатов
  ! TPRINT  -- шаг печати результатов 
	! HMIN -- минимальный возможный шаг интегрирования 
	! HMAX0 -- максимальный возможный шаг интегрирования 
	! HMAX -- максимальный возможный шаг интегрирования  между соседними точками разрыва
	! JSTART -- флаг для DIFSUB: 0 -- первый шаг (инициализация), 
	!                           -1 -- повторить последний шаг с новым H, 
	!                      >0 (NQ) -- шаг был успешен, продолжить интегрирование
	! KFLAG -- внешний флаг ошибки, переходит main -> solver -> difsub 
	! LDFLFG -- внутренний флаг ошибки, diffun, dac, deint, difsub
	! ISP, IEX -- флаг и индекс для печати в файл 

	
	
  
