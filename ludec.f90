!******************************************************
! UITG%PW = 1d-array(ALU)
! LU-decomposition of matrix ALU(N,N)
! UITG%IP - permutation vector  
!******************************************************
SUBROUTINE LUDEC(UITG, N, IER)
  USE COMMONS
	IMPLICIT NONE

	TYPE(TINTEG) 	:: UITG
  INTEGER N, IER

  REAL(8) T
  INTEGER I, J, K, KP1, NM1, M
  
  ! write PW(N*N) --> ALU(N,N)
  DO J = 1, N
    DO I = 1, N
      UITG%ALU(I, J) = UITG%PW((J - 1) * N + I)
    END DO
  END DO
  
  IER = 0 ! program stops if ier != 0
  UITG%IP(N) = 1
  IF (N .NE. 1) THEN
    NM1 = N - 1
    DO K = 1, NM1
      KP1 = K + 1
      M = K
      DO I = KP1, N
        IF (DABS(UITG%ALU(I, K)) .GT. DABS(UITG%ALU(M, K))) M = I
      END DO
      UITG%IP(K) = M
      T = UITG%ALU(M, K)
      IF (M .NE. K) THEN
        UITG%IP(N) = -UITG%IP(N)
        UITG%ALU(M,K) = UITG%ALU(K,K)
        UITG%ALU(K,K) = T
      END IF
      IF (T .EQ. 0.0D0) THEN
        IER = K
        UITG%IP(N) = 0
        RETURN
      END IF
      T = 1.D0 / T
      DO I = KP1, N
        UITG%ALU(I, K) = -UITG%ALU(I, K) * T
      END DO
      DO J = KP1, N
        T = UITG%ALU(M, J)
        UITG%ALU(M, J) = UITG%ALU(K, J)
        UITG%ALU(K, J) = T
        IF(T .NE. 0.D0) THEN
          DO I=KP1,N
            UITG%ALU(I, J) = UITG%ALU(I, J) + UITG%ALU(I, K) * T
          END DO
        END IF
      END DO
    END DO
  END IF
  K = N
  IF (UITG%ALU(N, N) .EQ. 0.) THEN
    IER = K
    UITG%IP(N) = 0
  END IF
  RETURN
END SUBROUTINE LUDEC
