!******************************************************
! initialize and solve dde  
! tos - start time, tfin - finish time 
! isp - print solution flag 
! iex - num of sol file to write results
! kflag - error flag 
!******************************************************
SUBROUTINE SOLVER(T0S, TFIN, ISP, KFLAG, IEX)
  USE COMMONS
	IMPLICIT NONE

  TYPE(TPROBLEM):: UPRB
  TYPE(TDISCPT)	:: UJMP
	TYPE(TWORK)		:: UWRK
	TYPE(TDELAY)	:: UDEL
	TYPE(TINTEG) 	:: UITG
	
	REAL(8), ALLOCATABLE :: Y(:,:)
  REAL(8) T0S, TFIN, TOUT, T, TPRINT, HMIN, HMAX, HMAX0
  INTEGER KFLAG, LDFLFG, I, ISP, IEX, JSTART, NPRINT
	
  CHARACTER(LEN = 9) :: FLNM
  CHARACTER(LEN = 100) :: MYFMT

!*==========================================================================
!************************ PARAMETERS SET BY USER ***************************	
	! INIT PROBLEM PARAMETERS
	UPRB%N = 3
	UPRB%NDEL = 4
	UPRB%NYD = 2
	UPRB%KPR = 1
	UDEL%L = (/ 10001, 5001 /)
	UJMP%MXJMP = 201
	ALLOCATE ( UPRB%MD (UPRB%N, UPRB%NDEL) )
	UPRB%MD = RESHAPE( (/  1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1 /) , SHAPE(UPRB%MD))
	CALL ALOCINI(UPRB, UJMP, UDEL, UITG) !ALLOCATES MEMORY AND INIT OTHER PROBLEM PARAMETERS
	UPRB%DELAY = (/ 1.0, 2.0, 0.5, 1.5 /) 

	! SET OUTPUT FILE AND FORMAT 
	MYFMT = '(1X, 1F12.5, 17E15.7)'
  IF (ISP.EQ.1) THEN
    WRITE(FLNM, "(A3,I1,A4)") "sol", IEX, ".dat"
    OPEN(UNIT=9,FILE=FLNM)
    REWIND 9
  END IF
	
	! INIT T, Y 
  UWRK%T0 = T0S
	ALLOCATE ( Y(8, UPRB%N) )
	Y(1, 1) = 1
  Y(1, 2) = 1
  Y(1, 3) = 1

 ! METHOD PARAMETERS 
  DO I=1,UPRB%N
    UITG%GROUND(I) = 1D-28
    UITG%YMAX(I) = DMAX1(Y(1, I), UITG%GROUND(I))
  END DO
  UITG%MF = 2
  UITG%MAXDER = 6
  UITG%EPS = 1.0D-6
  UITG%NCUT = 8
  HMIN = 1.0D-15
  HMAX0 = TFIN - UWRK%T0
  TPRINT = 1.00D2
	UWRK%H_W = HMIN 
!***************************************************************************	
!*==========================================================================

  ! FIND DISCONTINUITY POINTS OF DERIVATIVES 
	LDFLFG = 0
	UJMP%MAXD2 = UITG%MAXDER + 4
  UJMP%NJUMP = UJMP%MAXD2 * UPRB%NDEL + 1
  CALL PJUMP(UJMP, UWRK, UPRB%NDEL, UPRB%DELAY, UDEL%IDMAX) 
  JSTART = 0
  NPRINT = INT(UWRK%T0 / TPRINT) + 1
  UJMP%INTJ = 1
  DO I = 1, 2
    UDEL%NADEL(I) = 1
    UDEL%ISUCST(I) = 1
    UDEL%ICAPAC(I) = UDEL%L(I)
  END DO
  DO I = 1, UPRB%NDEL
    UDEL%NLASTW(I) = 1
    UDEL%NLAST(I) = 1
  END DO
  UWRK%DELMIN = UPRB%DELAY(UJMP%IDMIN)
  IF (UITG%MF .EQ. 1) HMAX0 = UWRK%DELMIN / 1.1D0
  HMAX = HMAX0
  TOUT = UWRK%T0
  T = UWRK%T0
  KFLAG = 0
	
	! START INTEGRATION 
  DO WHILE (TOUT .LT. TFIN)
    CALL SPEINT(UJMP, TPRINT, NPRINT, TOUT, HMAX0, HMAX, TFIN)
    CALL NQC(UJMP, T, UWRK%NQ, JSTART, UWRK%H_W, HMIN)
    DO WHILE (T .LT. TOUT)
      IF ((TOUT - T) .LE. UWRK%H_W) THEN
        UWRK%H_W = TOUT - T
      END IF
      CALL DIFSUB(UPRB, UDEL, UWRK, UITG, T, Y, HMIN, HMAX, KFLAG, JSTART, LDFLFG)
			IF (LDFLFG .NE. 0) KFLAG = 0
      SELECT CASE(KFLAG)
      CASE(-4)
        MYFMT = '(1X, " KFLAG = ", I2, " CANNOT REACH REQUIRED ERROR, NCUT = 8")'
        WRITE(*, MYFMT) KFLAG
        RETURN
      CASE(-3)
        MYFMT = '(1X, " KFLAG = ", I2, " CORRECTOR DOES NOT CONVERGE")'
        WRITE(*, MYFMT) KFLAG
        RETURN
      CASE(-2)
        MYFMT = '(1X, " KFLAG = ", I2, "  MAXDER .LT. 6 .OR. 7")'
        WRITE(*, MYFMT) KFLAG
        RETURN
      CASE(-1)
        MYFMT = '(1X," KFLAG = ", I2, " CANNOT REACH REQUIRED ERROR, H_W = HMIN")'
        WRITE(*, MYFMT) KFLAG
        RETURN
      CASE(0)
        MYFMT = '(1X, "KFLAG = ", I2, " UNKNOWN ERROR ")'
        WRITE(*, MYFMT) KFLAG
        RETURN
      CASE DEFAULT
      END SELECT
!*==========================================================================
!******************** WRITING TO FILES SET BY USER *************************  
	  IF (ISP .EQ. 1) THEN
        WRITE(9, MYFMT) T, (Y(1, I), I = 1, UPRB%N)
	  ENDIF
	  
    END DO
  END DO
	
	! WRITE RESULT IN TFIN 
  IF (ISP .EQ. 1) THEN 
		WRITE(*, '(" T = ", F6.2, 2X, 3D13.6)') T, Y(1,1), Y(1,2), Y(1,3)
		CLOSE(9)
	END IF 
!***************************************************************************	
!*==========================================================================
  RETURN
END SUBROUTINE SOLVER
