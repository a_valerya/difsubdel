!******************************************************
! calc delayed vars with initial or 
! by interpolation of previous y-values  
! IDEL - index of delay, YD - result
!******************************************************
! call: initial 
SUBROUTINE DEINT(UPRB, UDEL, UWRK, T, IDEL, Y, YD, LDFLFG)
  USE COMMONS
	IMPLICIT NONE
	
	TYPE(TPROBLEM):: UPRB
	TYPE(TWORK)		:: UWRK
	TYPE(TDELAY)	:: UDEL
	
  REAL(8) T
  INTEGER IDEL, LDFLFG 
  REAL(8) Y(8, UPRB%N)
  REAL(8) YD(UPRB%ILENG(IDEL))

  INTEGER N1, J , I, J2, IX, J1
  REAL(8) TZ, S, S1

  TZ = T - UPRB%DELAY(IDEL)
  IF (TZ .LE. UWRK%T0) THEN
    IF ((TZ .NE. UWRK%T0) .OR. (UWRK%INDJ .NE. 1)) THEN
			DO I = 1, UPRB%ILENG(IDEL)
				J = UPRB%IYD( UPRB%IVDS(I, IDEL) )
				CALL INITIAL(TZ, J, YD(I))
			END DO 
      RETURN
    END IF
  END IF
  IF (UPRB%DELAY(IDEL) .LT. UWRK%H_W) THEN
    DO J1 = 1, UPRB%ILENG(IDEL)
      J2 = UPRB%IVDS(J1, IDEL) !IVDS_I(J1)
      J2 = UPRB%IYD(J2)
      S = -UPRB%DELAY(IDEL) / UWRK%H_W
      S1 = 1.D0
      YD(J1) = Y(1, J2)
      DO J = 2, UWRK%K_W
        S1 = S * S1
        YD(J1) = YD(J1) + Y(J,J2) * S1
      END DO
    END DO
    UDEL%NLASTW(IDEL) = UDEL%ISUCST(1) - 1
    RETURN
  END IF
  N1 = UDEL%NLAST(IDEL)
  I = 0
  DO IX = N1, UDEL%L(1) - 1
    IF ((TZ .GE. UDEL%TDEL(IX)) .AND. (TZ .LE. UDEL%TDEL(IX + 1))) THEN
      I = IX
      EXIT
    END IF
  END DO
  IF (I .EQ. 0) THEN
    DO IX = 1, N1 - 1
      IF ((TZ .GE. UDEL%TDEL(IX)) .AND. (TZ .LE. UDEL%TDEL(IX + 1))) THEN
        I = IX
        EXIT
      END IF
    END DO
  END IF
  IF (I .EQ. 0) THEN
    WRITE(6, '(1X," Time node for interpolation not found T-delay=",D10.2)') T
    WRITE(6, *) TZ
    LDFLFG = -1
    RETURN
  END IF
  UDEL%NLASTW(IDEL) = I
  S = (TZ - UDEL%TDEL(I + 1)) / (UDEL%TDEL(I + 1) - UDEL%TDEL(I))
  DO J1 = 1, UPRB%ILENG(IDEL)
    J = UDEL%NADEL(I + 1) + (UPRB%IVDS(J1, IDEL) - 1) * UDEL%NQDEL(I + 1) ! IVDS_I(J1)
    YD(J1) = UDEL%YDEL(J)
    S1 = 1.D0
    DO J2 = 2, UDEL%NQDEL(I + 1)
      J = J + 1
      S1 = S * S1
      YD(J1) = YD(J1) + S1 * UDEL%YDEL(J)
    END DO
  END DO
  RETURN
END
